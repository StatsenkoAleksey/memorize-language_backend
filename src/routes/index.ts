import { Router }  from 'express'
const { Pool } = require('pg');
const { db } = require('../config/db')
const router = Router()

router.get('/', function(req, res) {
  
  res.status(200).send({ message: 'Hello world!' });
});

module.exports = router;
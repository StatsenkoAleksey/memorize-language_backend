"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const { Pool } = require('pg');
const { db } = require('../config/db');
const router = express_1.Router();
router.get('/', function (req, res) {
    res.status(200).send({ message: 'Hello world!' });
});
module.exports = router;
//# sourceMappingURL=index.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const postgres_migrations_1 = require("postgres-migrations");
const db_1 = require("../config/db");
const migrationFolder = 'migrations';
const onSuccess = (value) => {
    if (!value.length) {
        console.log('No new migrations found.');
        return;
    }
    console.log('Applying migrations:');
    value.map((v) => console.log(v.fileName));
};
postgres_migrations_1.migrate(db_1.db, migrationFolder)
    .then(onSuccess)
    .catch(e => console.error(e));
//# sourceMappingURL=migrate.js.map